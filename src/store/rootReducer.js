import { combineReducers } from "redux";
import testReducer from "./reducers/testReducer";
import asyncReducer from "./reducers/asyncReducer";

const rootReducer = combineReducers({
    test: testReducer,
    async: asyncReducer    
});

export default rootReducer;
