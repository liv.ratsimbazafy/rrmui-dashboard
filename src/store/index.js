import { createStore, applyMiddleware } from "redux";
import reduxPromise from "redux-promise"; // Error with async actions !!! switch to redux-thunk
import thunk from "redux-thunk";

import { persistStore } from "redux-persist";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { composeWithDevTools } from "redux-devtools-extension";

import rootReducer from "./rootReducer";

const persistConfig = {
    key: "rrmui",
    storage: storage,
    blacklist: ['async', 'test'],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(
    persistedReducer,
    composeWithDevTools(applyMiddleware(thunk))
);

const persiStore = persistStore(store);

export { store, persiStore };
