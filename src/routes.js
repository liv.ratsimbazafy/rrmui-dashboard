import "simplebar/src/simplebar.css";

import { useRoutes } from "react-router-dom";

import Landing from '../pages/Landing'


export default function Router() {
    return useRoutes([       
        {
            path: "/", element: <Landing />
        }      
    ]);
}
